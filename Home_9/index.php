<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

/** Tasks 3.1 - 3.2 */

/**
 * Adapter
 */

$HDMIport1 = new task_3_adapter\HDMI();
$HDMIport1->showPictures();

$HDMIAdapter1 = new task_3_adapter\HDMIAdapter($HDMIport1);
$HDMIAdapter1->transferInfo();

/**
 * Bridge
 */

$boeing1 = new task_3_bridge\Boeing();
$pilot1 = new task_3_bridge\Pilot($boeing1);
$pilot1->getVehicle();

$taxi1 = new task_3_bridge\BlackCab();
$driver1 = new task_3_bridge\TaxiDriver($taxi1);
$driver1->getVehicle();

/**
 * Composite
 */

$drink = new task_3_composite\Machine();
$coffee = new task_3_composite\Coffee();
$water = new task_3_composite\Water();
$foam = new task_3_composite\Foam();
$cream = new task_3_composite\Cream();

$drink->add($coffee);
$drink->add($water);
$drink->remove($water);
$drink->add($foam);

$drink->getDrink();

/**
 * Decorator
 */

$device1 = new task_3_decorator\Iphone11Pro();
$device1->getEquipment();
echo '<br>';

$device2 = new \task_3_decorator\IphoneWithCase($device1);
$device2->getEquipment();
echo '<br>';

$device3 = new \task_3_decorator\IphoneWithGlass($device1);
$device3->getEquipment();
echo '<br>';

/**
 * Facade
 */

$aircraft1 = new task_3_facade\Boeing(new task_3_facade\Aircraft());
$aircraft1->takeOff();

/**
 * Flyweight
 */

$cities = new task_3_flyweight\DestinationCity();
$novaPoshta = new task_3_flyweight\NovaPoshta($cities);

$novaPoshta->takeOrder('Kyiv',1234);
$novaPoshta->takeOrder('Kyiv',7530);
$novaPoshta->takeOrder('Lviv',1590);
$novaPoshta->takeOrder('Lviv',9632);

$novaPoshta->showOrders();

/**
 * Proxy
 */

$camera1 = new task_3_proxy\Camera();
$camera1->filmVideos();
echo '<br>';

$camera2 = new task_3_proxy\TraficControl(new task_3_proxy\Camera());
$camera2->controlSpeedLimits();

/** Tasks 4.1 - 4.8 */

/**
 * Фондовая биржа присылает вашему приложению данные по
 * котировкам акций в формате xml. Ваше приложение для
 * работы использует библиотеку, которая понимает только
 * json-формат данных. Вам нужно “подружить” все части
 * приложения.
 */

$XML = new task_4_2\XML();
$XML->getInfo();

$JSON = new task_4_2\XMLToJSONAdapter($XML);
$JSON->getInfo();

/**
 * У вас есть пирамида, куб и шар, которые могут быть
 * зеленого, черного и белого цветов. Нужно максимум
 * оптимально дать возможность в системе создавать все
 * вариации цветов и форм.
 */

$color1 = new task_4_3\Green();
$color2 = new task_4_3\Black();
$color3 = new task_4_3\White();

$figure1 = new task_4_3\Pyramid($color1);
$figure2 = new task_4_3\Cube($color2);
$figure3 = new task_4_3\Ball($color3);

$figure1->getFigure();
$figure2->getFigure();
$figure3->getFigure();

/**
 * Ваша компания занимается сортировкой и передачей разного
 * рода вещей Красному кресту. Вам нужно написать программу
 * для робота, который разбирает коробки и отправляет вещи
 * на переработку или Красному кресту. Имейте ввиду, что
 * внутри большой изначальной коробки могут быть как и вещи,
 * так и коробки поменьше, в которых в свою очередь могут
 * быть и вещи и коробки и тд.
 */

//Composite
/**
 * TODO: Если честно, так и не въехала как сделать((
 */

/**
 * Вам нужно отправлять уведомления на почту, СМСкой, в
 * скайп-бот, в телеграм-бот и звонком на телефон. Постройте
 * систему таких уведомлений, учитывая возможность
 * группировать способы отправки уведомлений в разных к-твах
 * и вариациях.
 */

$notification1 = new task_4_5\TelegramNotification();
$notification1->sendNotification();
$notification2 = new task_4_5\TelegramAndSmsNotification($notification1);
$notification2->sendNotification();

$notification3 = new task_4_5\SkypeNotification();
$notification3->sendNotification();
$notification4= new task_4_5\SkypeAndCallNotification($notification3);
$notification4->sendNotification();

/**
 * Сделайте систему обработки и доставки заказа Интернет-магазина,
 * начиная от звонка клиента и заканчивая получением им товара.
 */

$store1 = new task_4_6\Store();
$manager1 = new task_4_6\StoreManager($store1);
$manager1->buyProduct();

/**
 * Оптимизируйте работу игры, в которой каждую секунду
 * используется очень много разных интерактивных элементов
 * и на каждый создается объект. Из логов вы знаете, что
 * элементы: координаты, векторы и скорость - меняются
 * очень часто, но занимают очень мало памяти в системе.
 * А текстуры и их цвета - занимают очень много места, но
 * статичны.
 */

$staticController = new task_4_7\StaticController();
$graphic = new task_4_7\Graphic($staticController);

$graphic->saveStaticInto('texture','grass');
$graphic->saveStaticInto('texture','sky');
$graphic->saveStaticInto('color','green');
$graphic->saveStaticInto('color','blue');

$graphic->showStaticInto();

/**
 * Создайте платежный терминал, с помощью которого можно
 * будет купить разные товары. Учтите то, что оплата
 * наличкой в вашей компании приветствуется и происходит
 * максимум просто и без задержек. А оплата через терминал,
 * связана с доп налогами и проверкой платежа, плюс еще
 * компания решила сделать доп наценку 0.5% за товар при
 * расчете картой.
 */

/**
 * Version #1
 */

echo '<b>Payment by cash</b><br>';
$payment1 = new task_4_8_1\Cash();
$payment1->pay(500);

echo '<b>Payment by a credit card</b><br>';
$payment2 = new task_4_8_1\CreditCard(new task_4_8_1\Cash());
$payment2->pay(500);

/**
 * Version #2
 */

echo '<b>Payment by cash</b><br>';
$method1 = new task_4_8_2\Cash();
$payment3 = new task_4_8_2\Payment($method1);
$payment3->pay(600);

echo '<b>Payment by a credit card</b><br>';
$method2 = new task_4_8_2\CreditCard();
$payment4 = new task_4_8_2\Payment($method2);
$payment4->pay(600);

/** Task 8.1 */

//TODO: Сделал!

/** Tasks 9.1 - 9.3 */

/**
 * TODO: Смог только (вроде) сделать пункт 9,3.
 * TODO: Сконфигурировать бд не смог, только в 22:00 дали свет)
 * TODO: Уже тупо не успевал(
 */

/** Task 10.1 */

/**
 * TODO: так, ка не сделал 9 задание, это тоже не получилось сделать(
 */
