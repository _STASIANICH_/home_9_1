<?php

declare(strict_types=1);

namespace task_3_adapter;

class HDMI
{
    public function showPictures()
    {
        echo 'HDMI port is showing pictures...<br>';
    }
}
