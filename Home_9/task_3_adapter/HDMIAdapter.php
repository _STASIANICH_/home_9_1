<?php

declare(strict_types=1);

namespace task_3_adapter;

class HDMIAdapter implements USBInterface
{
    protected $HDMI;

    public function __construct(HDMI $port)
    {
        $this->HDMI = $port;
    }

    public function transferInfo()
    {
        echo 'HDMI is now connected to the PC by an USB port!<br>';
    }
}
