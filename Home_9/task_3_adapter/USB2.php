<?php

declare(strict_types=1);

namespace task_3_adapter;

class USB2 implements USBInterface
{
    public function transferInfo()
    {
        echo 'USB 2.0 transfers info at 480 Mbit/s speed<br>';
    }
}
