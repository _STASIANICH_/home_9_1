<?php

declare(strict_types=1);

namespace task_3_adapter;

class USB3 implements USBInterface
{
    public function transferInfo()
    {
        echo 'USB 3.0 transfers info at 5 Gbit/s speed<br>';
    }
}
