<?php

declare(strict_types=1);

namespace task_3_adapter;

interface USBInterface
{
    public function transferInfo();
}
