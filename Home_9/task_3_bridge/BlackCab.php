<?php

declare(strict_types=1);

namespace task_3_bridge;

class BlackCab implements Vehicle
{
    public function getVehicle()
    {
        return 'Black Cabs';
    }
}
