<?php

declare(strict_types=1);

namespace task_3_bridge;

class Boeing implements Vehicle
{
    public function getVehicle()
    {
        return 'Boeing aircrafts';
    }
}
