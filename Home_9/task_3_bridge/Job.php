<?php

declare(strict_types=1);

namespace task_3_bridge;

interface Job
{
    public function __construct(Vehicle $vehicle);
    public function getVehicle();
}
