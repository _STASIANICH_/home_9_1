<?php

declare(strict_types=1);

namespace task_3_bridge;

class Pilot implements Job
{
    protected $vehicle;

    public function __construct(Vehicle $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    public function getVehicle()
    {
        echo 'Pilots use ' . $this->vehicle->getVehicle();
        echo ' for their jod!<br>';
    }
}
