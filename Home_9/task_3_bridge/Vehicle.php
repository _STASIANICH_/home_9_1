<?php

declare(strict_types=1);

namespace task_3_bridge;

interface Vehicle
{
    public function getVehicle();
}
