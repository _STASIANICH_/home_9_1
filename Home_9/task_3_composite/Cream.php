<?php

declare(strict_types=1);

namespace task_3_composite;

class Cream implements DrinkInterface
{
    public function makeDrink()
    {
        return 'сливки';
    }
}
