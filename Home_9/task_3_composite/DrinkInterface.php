<?php

declare(strict_types=1);

namespace task_3_composite;

interface DrinkInterface
{
    public function makeDrink();
}
