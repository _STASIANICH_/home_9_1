<?php

declare(strict_types=1);

namespace task_3_composite;

class Foam implements DrinkInterface
{
    public function makeDrink()
    {
        return 'молочная пенка';
    }
}
