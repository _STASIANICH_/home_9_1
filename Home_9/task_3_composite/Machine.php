<?php

declare(strict_types=1);

namespace task_3_composite;

class Machine
{
    protected array $components = [];

    public function add($component)
    {
        $this->components[] = $component;
    }

    public function remove($component)
    {
        foreach ($this->components as $key=>$ingredient){
            if($component == $this->components[$key]){
                unset($this->components[$key]);
            }
        }
    }

    public function getDrink()
    {
        echo '<b>This drink has:</b><br>';
        foreach ($this->components as $ingredient){
            echo '* ' . $ingredient->makeDrink() . '<br>';
        }
    }
}
