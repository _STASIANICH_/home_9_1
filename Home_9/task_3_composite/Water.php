<?php

declare(strict_types=1);

namespace task_3_composite;

class Water implements DrinkInterface
{
    public function makeDrink()
    {
        return 'вода';
    }
}
