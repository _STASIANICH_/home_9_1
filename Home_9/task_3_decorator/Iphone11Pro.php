<?php

declare(strict_types=1);

namespace task_3_decorator;

class Iphone11Pro implements Smartphone
{
    public function getEquipment()
    {
        echo 'The New Iphone 11 Pro';
    }
}