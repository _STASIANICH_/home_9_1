<?php

declare(strict_types=1);

namespace task_3_decorator;

class IphoneWithGlass implements Smartphone
{
    protected $smartphone;

    public function __construct(Smartphone $device)
    {
        $this->smartphone = $device;
    }

    public function getEquipment()
    {
        echo $this->smartphone->getEquipment() . ' with a glass.';
    }
}