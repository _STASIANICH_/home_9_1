<?php

declare(strict_types=1);

namespace task_3_decorator;

interface Smartphone
{
    public function getEquipment();
}
