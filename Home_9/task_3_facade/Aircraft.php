<?php

declare(strict_types=1);

namespace task_3_facade;

class Aircraft
{
    public function turnOnEngines()
    {
        echo 'Engines are starting...<br>';
    }

    public function checkComponents()
    {
        echo 'The crew is checking components...<br>';
    }

    public function fillFuel()
    {
        echo 'The tanks are filling with the fuel...<br>';
    }

    public function takeOff()
    {
        echo 'The aircraft is taking off...<br>';
    }
}
