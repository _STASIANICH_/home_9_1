<?php

declare(strict_types=1);

namespace task_3_facade;

class Boeing
{
    protected $aircraft;

    public function __construct(Aircraft $aircraft)
    {
        $this->aircraft = $aircraft;
    }

    public function takeOff()
    {
        $this->aircraft->fillFuel();
        $this->aircraft->turnOnEngines();
        $this->aircraft->checkComponents();
        $this->aircraft->takeOff();
    }
}
