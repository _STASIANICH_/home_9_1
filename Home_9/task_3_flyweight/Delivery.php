<?php

declare(strict_types=1);

namespace task_3_flyweight;

class Delivery
{
    protected int $orderID;
    protected string $city;

    public function __construct($orderID, $city)
    {
        $this->orderID = $orderID;
        $this->city = $city;
    }

    public function showDetails()
    {
        echo 'Order #' . $this->orderID . ' to ' . $this->city . '<br>';
    }

    public function showCity()
    {
        return $this->city;
    }
}
