<?php

declare(strict_types=1);

namespace task_3_flyweight;

class DestinationCity
{
    protected $orders = [];

    public function addOrder($orderID, $city)
    {
        if (empty($this->orders[$city])){
            $this->orders[$city] = new Delivery($orderID, $city);
        }

        return $this->orders[$city];
    }
}
