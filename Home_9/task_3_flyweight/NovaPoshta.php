<?php

declare(strict_types=1);

namespace task_3_flyweight;

class NovaPoshta
{
    protected $orderList;
    protected $destinations;

    public function __construct(DestinationCity $city)
    {
        $this->destinations = $city;
    }

    public function takeOrder(string $city, int $orderID)
    {
        $this->orderList[$orderID] = $this->destinations->addOrder($orderID, $city);
    }

    public function showOrders()
    {
        foreach ($this->orderList as $orderID=>$order){
            echo $orderID . ' => ' . $this->orderList[$orderID]->showCity() . '<br>';
        }
    }
}
