<?php

declare(strict_types=1);

namespace task_3_proxy;

class Camera
{
    public function filmVideos()
    {
        echo 'The camera is filming now... ';
    }
}

