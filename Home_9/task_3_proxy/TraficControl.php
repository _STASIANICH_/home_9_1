<?php

declare(strict_types=1);

namespace task_3_proxy;

class TraficControl
{
    protected $camera;

    public function __construct(Camera $camera)
    {
        $this->camera = $camera;
    }

    public function controlSpeedLimits()
    {
        $this->camera->filmVideos();
        echo 'and controlling speed limits...<br>';
    }
}

