<?php

declare(strict_types=1);

namespace task_4_2;

class XMLToJSONAdapter implements dataInterface
{
    protected $XML;

    public function __construct(XML $data)
    {
        $this->XML = $data;
    }

    public function getInfo()
    {
        echo 'Changing XML data into JSON...<br>';
    }
}
