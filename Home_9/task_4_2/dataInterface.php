<?php

declare(strict_types=1);

namespace task_4_2;

interface dataInterface
{
    public function getInfo();
}
