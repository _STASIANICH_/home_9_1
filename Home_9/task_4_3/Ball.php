<?php

declare(strict_types=1);

namespace task_4_3;

class Ball implements FigureInteface
{
    protected $color;

    public function __construct(ColorInterface $color)
    {
        $this->color = $color;
    }

    public function getFigure()
    {
        echo $this->color->getColor() . ' ball.<br>';
    }
}
