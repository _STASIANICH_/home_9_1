<?php

declare(strict_types=1);

namespace task_4_3;

class Black implements ColorInterface
{
    public function getColor()
    {
        return 'Black';
    }
}
