<?php

declare(strict_types=1);

namespace task_4_3;

interface ColorInterface
{
    public function getColor();
}
