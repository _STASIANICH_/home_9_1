<?php

declare(strict_types=1);

namespace task_4_3;

interface FigureInteface
{
    public function __construct(ColorInterface $color);
    public function getFigure();
}
