<?php

declare(strict_types=1);

namespace task_4_5;

class EmailNotification implements NotificationInterface
{
    public function sendNotification()
    {
        echo 'Sending notification by email...<br>';
    }
}