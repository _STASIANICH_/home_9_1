<?php

declare(strict_types=1);

namespace task_4_5;

interface NotificationInterface
{
    public function sendNotification();
}
