<?php

declare(strict_types=1);

namespace task_4_5;

class PhoneCallNotification implements NotificationInterface
{
    public function sendNotification()
    {
        echo 'Sending notification by calling...<br>';
    }
}