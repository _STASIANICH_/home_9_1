<?php

declare(strict_types=1);

namespace task_4_5;

class SkypeAndCallNotification implements NotificationInterface
{
    protected $notification;

    public function __construct(NotificationInterface $method)
    {
        $this->notification = $method;
    }
    public function sendNotification()
    {
        $this->notification->sendNotification();
        echo 'and by calling ...<br>';
    }
}