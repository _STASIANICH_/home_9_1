<?php

declare(strict_types=1);

namespace task_4_5;

class SkypeNotification implements NotificationInterface
{
    public function sendNotification()
    {
        echo 'Sending notification by skype...<br>';
    }
}