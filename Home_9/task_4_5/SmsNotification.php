<?php

declare(strict_types=1);

namespace task_4_5;

class SmsNotification implements NotificationInterface
{
    public function sendNotification()
    {
        echo 'Sending notification by sms...<br>';
    }
}