<?php

declare(strict_types=1);

namespace task_4_5;

class TelegramNotification implements NotificationInterface
{
    public function sendNotification()
    {
        echo 'Sending notification by telegram...<br>';
    }
}