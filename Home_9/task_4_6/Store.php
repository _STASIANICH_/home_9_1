<?php

declare(strict_types=1);

namespace task_4_6;

class Store
{
    public function makeOrder()
    {
        echo 'User is making the order...<br>';
    }

    public function findInWarehouse()
    {
        echo 'Finding the order in warehouse...<br>';
    }

    public function collectOrder()
    {
        echo 'The order is collecting...<br>';
    }

    public function sendToUser()
    {
        echo 'The order is sending to user...<br>';
    }
}
