<?php

declare(strict_types=1);

namespace task_4_6;

class StoreManager
{
    protected $store;

    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    public function buyProduct()
    {
        $this->store->makeOrder();
        $this->store->findInWarehouse();
        $this->store->collectOrder();
        $this->store->sendToUser();
    }
}
