<?php

declare(strict_types=1);

namespace task_4_7;

class Element
{
    protected string $name;
    protected string $value;

    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    public function showType()
    {
        return $this->name;
    }
}
