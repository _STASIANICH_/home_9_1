<?php

declare(strict_types=1);

namespace task_4_7;

class Graphic
{
    protected $staticElements = ['texture', 'color'];
    protected $staticInto;
    protected $staticControler;

    public function __construct(StaticController $controller)
    {
        $this->staticControler = $controller;
    }

    public function saveStaticInto($element, $value)
    {
        foreach ($this->staticElements as $staticElement){
            if($element == $staticElement){
                $this->staticInto[$value] = $this->staticControler->saveInfo($element, $value);
            }
        }
    }

    public function showStaticInto()
    {
        foreach ($this->staticInto as $name=>$element){
            echo $name . ' => ' . $this->staticInto[$name]->showType() . '<br>';
        }
    }
}
