<?php

declare(strict_types=1);

namespace task_4_7;

class StaticController
{
    protected $staticInfo = [];

    public function saveInfo($element, $value)
    {
        if (empty($this->staticInfo[$value])){
            $this->staticInfo[$value] = new Element($element, $value);
        }

        return $this->staticInfo[$value];
    }
}
