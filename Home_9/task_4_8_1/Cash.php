<?php

declare(strict_types=1);

namespace task_4_8_1;

class Cash implements Payment
{
    public function pay($amount)
    {
        echo '-' . $amount . '$<br>';
    }
}

