<?php

declare(strict_types=1);

namespace task_4_8_1;

class CreditCard implements Payment
{
    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function pay($amount)
    {
        if ($this->isValid($amount)) {
            $sum = $amount * 1.005;
            $this->payment->pay($sum);
        }else{
            echo '<b>Payment error! Contact your bank!</b>';
        }
    }

    protected function isValid($sum)
    {
        return $sum < 10000 ?? false;
    }
}

