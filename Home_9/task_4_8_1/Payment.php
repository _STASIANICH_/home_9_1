<?php

declare(strict_types=1);

namespace task_4_8_1;

interface Payment
{
    public function pay($amount);
}
