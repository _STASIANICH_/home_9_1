<?php

declare(strict_types=1);

namespace task_4_8_2;

interface Bank
{
    public function pay($amount);
}
