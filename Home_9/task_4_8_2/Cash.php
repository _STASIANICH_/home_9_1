<?php

declare(strict_types=1);

namespace task_4_8_2;

class Cash implements Bank
{
    public function pay($amount)
    {
        echo '-' . $amount . '$<br>';
    }
}

