<?php

declare(strict_types=1);

namespace task_4_8_2;

class CreditCard implements Bank
{
    public function pay($amount)
    {
        if ($this->isValid($amount)) {
            echo '-' . $amount * 1.005 . '$<br>';
        }else{
            echo '<b>Payment error! Contact your bank!</b>';
        }
    }

    protected function isValid($sum)
    {
        return $sum < 10000 ?? false;
    }
}

