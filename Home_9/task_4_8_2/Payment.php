<?php

declare(strict_types=1);

namespace task_4_8_2;

class Payment
{
    protected $paymentMethod;

    public function __construct(Bank $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function pay($amount)
    {
        $this->paymentMethod->pay($amount);
    }
}
