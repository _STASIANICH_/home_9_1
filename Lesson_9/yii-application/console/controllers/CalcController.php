<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

class CalcController extends Controller
{
    public function actionPlus($value1 , $value2)
    {
        $res = intval($value1) + intval($value2);
        $value1 = $this->ansiFormat($value1, Console::FG_RED);
        $value2 = $this->ansiFormat($value2, Console::FG_YELLOW);
        $res = $this->ansiFormat($res, Console::FG_GREEN);
        echo $value1 . ' + ' . $value2 . ' = ' . $res . PHP_EOL;
    }

    public function actionMinus($value1 , $value2)
    {
        $res = intval($value1) - intval($value2);
        $value1 = $this->ansiFormat($value1, Console::FG_RED);
        $value2 = $this->ansiFormat($value2, Console::FG_YELLOW);
        $res = $this->ansiFormat($res, Console::FG_GREEN);
        echo $value1 . ' - ' . $value2 . ' = ' . $res . PHP_EOL;
    }

    public function actionMultiply($value1 , $value2)
    {
        $res = intval($value1) * intval($value2);
        $value1 = $this->ansiFormat($value1, Console::FG_RED);
        $value2 = $this->ansiFormat($value2, Console::FG_YELLOW);
        $res = $this->ansiFormat($res, Console::FG_GREEN);
        echo $value1 . ' * ' . $value2 . ' = ' . $res . PHP_EOL;
    }

    public function actionDivide($value1 , $value2)
    {
        $res = intval($value1) / intval($value2);
        $value1 = $this->ansiFormat($value1, Console::FG_RED);
        $value2 = $this->ansiFormat($value2, Console::FG_YELLOW);
        $res = $this->ansiFormat($res, Console::FG_GREEN);
        echo $value1 . ' / ' . $value2 . ' = ' . $res . PHP_EOL;
    }
}