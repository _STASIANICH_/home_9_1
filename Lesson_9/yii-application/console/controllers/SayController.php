<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

class SayController extends Controller
{
    public function actionSayWorld()
    {
        echo 'Hello, World!' . PHP_EOL;
    }

    public function actionSayInput($value)
    {
        $value = $this->ansiFormat($value, Console::FG_YELLOW);
        $uaFlag = $this->ansiFormat('#', Console::FG_YELLOW);
        $uaFlag .= $this->ansiFormat('#', Console::FG_BLUE);
        echo $this->ansiFormat('Hello, my name is ', Console::FG_BLUE) . $value . '. ' . $uaFlag . PHP_EOL;
    }
}